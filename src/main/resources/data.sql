create table bank_account(
	id integer not null,
	amount float not null,
	primary key(id)
);

INSERT INTO bank_account(ID, AMOUNT) VALUES (1, 1000);
INSERT INTO bank_account(ID, AMOUNT) VALUES (2, 2000);
INSERT INTO bank_account(ID, AMOUNT) VALUES (3, 3000);