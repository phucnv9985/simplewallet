package com.demo.simplewallet.entity;


public class BankAccount {
	private int id;
	private float amount;

	public BankAccount() {
	}

	public BankAccount(int id, float amount) {
		this.id = id;
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "\nBankAccount [id=" + id + ", amount=" + amount + "]";
	}

}
