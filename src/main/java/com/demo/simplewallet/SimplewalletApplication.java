package com.demo.simplewallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Simple Wallet Exercise(Transfer money between two accounts.)

1. Use database as the backend to preserve the account data
2. Make sure there’s no concurrent issue when transfer money from A account to B account
3. Make sure there’s no deadlock when transfer money from A to B, and at the same time there’s transfer from B to A
4. Write any kind of testing you think you need to prove 2 and 3
 */
@SpringBootApplication
public class SimplewalletApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SimplewalletApplication.class, args);
	}

}
