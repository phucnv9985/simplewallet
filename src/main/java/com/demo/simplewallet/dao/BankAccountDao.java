package com.demo.simplewallet.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.demo.simplewallet.entity.BankAccount;


@Repository
public class BankAccountDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Map<Integer, BankAccount> findById(int id, int... ids) {
		try {
			String idsStr = "" + id;
			for (int i : ids) {
				idsStr += "," + i;
			}
			List<BankAccount> rs = jdbcTemplate.query("select * from bank_account where id IN (" + idsStr + ") for update",
					new BeanPropertyRowMapper<BankAccount>(BankAccount.class));
			Map<Integer, BankAccount> result = new HashMap<>();
			for (BankAccount r : rs) {
				result.put(r.getId(), r);
			}
			return result;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public int update(BankAccount bankAccount) {
		return jdbcTemplate.update("update bank_account set amount = ? where id = ?",
				new Object[] { bankAccount.getAmount(), bankAccount.getId() });
	}

}
