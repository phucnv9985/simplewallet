package com.demo.simplewallet.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.demo.simplewallet.dao.BankAccountDao;
import com.demo.simplewallet.entity.BankAccount;

@Service
public class MoneyTransferService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private PlatformTransactionManager platformTransactionManager;
	@Autowired
	private BankAccountDao bankAccountDao;

	/**
	 * 
	 * @param debitAccount
	 * @param creditAccount
	 * @param money
	 * @return
	 */
	public String transfer(BankAccount debitAccount, BankAccount creditAccount, float money) /* throws Exception */ {
		DefaultTransactionDefinition definition = new DefaultTransactionDefinition();
		definition.setName("transferTransaction");
		TransactionStatus status = platformTransactionManager.getTransaction(definition);
		try {
			// lock table so that only one transaction is allowed to update data to the table 
			Map<Integer, BankAccount> dataFromDb = bankAccountDao.findById(debitAccount.getId(), creditAccount.getId());
			BankAccount debitAccountFromDb = dataFromDb.get(debitAccount.getId());
			BankAccount creditAccountFromDb = dataFromDb.get(creditAccount.getId());
			if (debitAccountFromDb == null) {
				platformTransactionManager.rollback(status);
				return "Account " + debitAccount.getId() + " doesn't exist in DB.";
			} else if (creditAccountFromDb == null) {
				platformTransactionManager.rollback(status);
				return "Account " + creditAccount.getId() + " doesn't exist in DB.";
			}
			logger.info("Beginning of transfering money");

			// check whether debitAccountFromDb.amount is larger than money
			if (debitAccountFromDb.getAmount() < money) {
				platformTransactionManager.rollback(status);
				return debitAccount.getId() + ": balance is not enough to transfer";
			}
			// subtract money from debitAccount 
			debitAccountFromDb.setAmount(debitAccountFromDb.getAmount() - money);
			bankAccountDao.update(debitAccountFromDb);

			logger.info("updated: {}", debitAccountFromDb);
			// sleep 100 milliseconds so that deadlock can occur.
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
			logger.info("after {} milliseconds.", 100);
			// add money to creditAccount
			creditAccountFromDb.setAmount(creditAccountFromDb.getAmount() + money);
			bankAccountDao.update(creditAccountFromDb);

			logger.info("updated: {}", creditAccountFromDb);

			platformTransactionManager.commit(status);
			return "success";
		} catch (Exception ex) {
			platformTransactionManager.rollback(status);
			return "rolled-back";
//			throw new Exception(ex);
		}
	}
}