package com.demo.simplewallet.service;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.assertj.core.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.demo.simplewallet.dao.BankAccountDao;
import com.demo.simplewallet.entity.BankAccount;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.profiles.active=test")
@AutoConfigureMockMvc
public class MoneyTransferServiceTest {

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	BankAccountDao bankAccountDao;
	@Autowired
	MoneyTransferService moneyTransferService;

	/*
	 * 1. Case: transfer money concurrently: A -> B; C -> B
	 */
	@Test
	public void testTransferA2BC2B() {
		BankAccount a = new BankAccount(1, 1000);
		BankAccount b = new BankAccount(2, 2000);
		BankAccount c = new BankAccount(3, 3000);
		insertOrUpdateData(a, b, c);
		float a2b = 10;
		float c2b = 20;
		transferA2BC2D(a, b, c, b, a2b, c2b);
		Map<Integer, BankAccount> bankAccountMapFromDB = bankAccountDao.findById(a.getId(), b.getId(), c.getId());

		Set<Float> actual = new HashSet<>();
		actual.add(bankAccountMapFromDB.get(a.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(b.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(c.getId()).getAmount());
		Set<Float> expected = new HashSet<>();
		expected.add(990.0f);
		expected.add(2030.0f);
		expected.add(2980.0f);
		assertArrayEquals(Arrays.array(expected), Arrays.array(actual));

	}

	/*
	 * 2. Case: transfer money concurrently: A -> B; B -> A
	 */
	@Test
	public void testTransferA2BB2A() {
		BankAccount a = new BankAccount(1, 1000);
		BankAccount b = new BankAccount(2, 2000);
		insertOrUpdateData(a, b);
		float a2b = 10;
		float b2a = 20;
		transferA2BC2D(a, b, b, a, a2b, b2a);
		Map<Integer, BankAccount> bankAccountMapFromDB = bankAccountDao.findById(a.getId(), b.getId());
		Set<Float> actual = new HashSet<>();
		actual.add(bankAccountMapFromDB.get(a.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(b.getId()).getAmount());
		Set<Float> expected = new HashSet<>();
		expected.add(1010.0f);
		expected.add(1990.0f);
		assertArrayEquals(Arrays.array(expected), Arrays.array(actual));

	}

	/*
	 * 3. Case: transfer money concurrently: A -> B; B -> C
	 */
	@Test
	public void testTransferA2BB2C() {
		BankAccount a = new BankAccount(1, 1000);
		BankAccount b = new BankAccount(2, 2000);
		BankAccount c = new BankAccount(3, 3000);
		insertOrUpdateData(a, b, c);
		float a2b = 10;
		float b2c = 20;
		transferA2BC2D(a, b, b, c, a2b, b2c);
		Map<Integer, BankAccount> bankAccountMapFromDB = bankAccountDao.findById(a.getId(), b.getId(), c.getId());

		Set<Float> actual = new HashSet<>();
		actual.add(bankAccountMapFromDB.get(a.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(b.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(c.getId()).getAmount());
		Set<Float> expected = new HashSet<>();
		expected.add(990.0f);
		expected.add(1990.0f);
		expected.add(3020.0f);
		assertArrayEquals(Arrays.array(expected), Arrays.array(actual));

	}

	/*
	 * 4. Case: transfer money concurrently: A -> B; B -> C; C -> A
	 */
	@Test
	public void testTransferA2BB2CC2A() {
		BankAccount a = new BankAccount(1, 1000);
		BankAccount b = new BankAccount(2, 2000);
		BankAccount c = new BankAccount(3, 3000);
		insertOrUpdateData(a, b, c);
		float a2b = 10;
		float b2c = 20;
		float c2a = 30;
		transferA2BC2DE2F(a, b, b, c, c, a, a2b, b2c, c2a);
		Map<Integer, BankAccount> bankAccountMapFromDB = bankAccountDao.findById(a.getId(), b.getId(), c.getId());

		Set<Float> actual = new HashSet<>();
		actual.add(bankAccountMapFromDB.get(a.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(b.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(c.getId()).getAmount());
		Set<Float> expected = new HashSet<>();
		expected.add(1020.0f);
		expected.add(1990.0f);
		expected.add(2990.0f);
		assertArrayEquals(Arrays.array(expected), Arrays.array(actual));

	}

	/*
	 * 5. Case: transfer money concurrently: A -> B; B -> A; C -> A
	 */
	@Test
	public void testTransferA2BB2AC2A() {
		BankAccount a = new BankAccount(1, 1000);
		BankAccount b = new BankAccount(2, 2000);
		BankAccount c = new BankAccount(3, 3000);
		insertOrUpdateData(a, b, c);
		float a2b = 10;
		float b2a = 20;
		float c2a = 30;
		transferA2BC2DE2F(a, b, b, a, c, a, a2b, b2a, c2a);
		Map<Integer, BankAccount> bankAccountMapFromDB = bankAccountDao.findById(a.getId(), b.getId(), c.getId());

		Set<Float> actual = new HashSet<>();
		actual.add(bankAccountMapFromDB.get(a.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(b.getId()).getAmount());
		actual.add(bankAccountMapFromDB.get(c.getId()).getAmount());
		Set<Float> expected = new HashSet<>();
		expected.add(1040.0f);
		expected.add(1990.0f);
		expected.add(2970.0f);
		assertArrayEquals(Arrays.array(expected), Arrays.array(actual));

	}

	private void transferA2BC2D(BankAccount a, BankAccount b, BankAccount c, BankAccount d, float a2b, float c2d) {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		Future<String> a2bFuture = executor.submit(new Callable<String>() {
			@Override
			public String call() throws Exception {
				return moneyTransferService.transfer(a, b, a2b);
			}
		});
		Future<String> c2dFuture = executor.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				return moneyTransferService.transfer(c, d, c2d);
			}
		});
		try {
			System.out.println(a2bFuture.get());
			System.out.println(c2dFuture.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	private void transferA2BC2DE2F(BankAccount a, BankAccount b, BankAccount c, BankAccount d, BankAccount e,
			BankAccount f, float a2b, float c2d, float e2f) {
		ExecutorService executor = Executors.newFixedThreadPool(3);
		Future<String> a2bFuture = executor.submit(new Callable<String>() {
			@Override
			public String call() throws Exception {
				return moneyTransferService.transfer(a, b, a2b);
			}
		});
		Future<String> c2dFuture = executor.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				return moneyTransferService.transfer(c, d, c2d);
			}
		});
		Future<String> e2fFuture = executor.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				return moneyTransferService.transfer(e, f, e2f);
			}
		});
		try {
			System.out.println(a2bFuture.get());
			System.out.println(c2dFuture.get());
			System.out.println(e2fFuture.get());
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		} catch (ExecutionException ex) {
			ex.printStackTrace();
		}
	}

	private void insertOrUpdateData(BankAccount... bankAccounts) {
		for (BankAccount bankAccount : bankAccounts) {
			jdbcTemplate.update("INSERT INTO bank_account(ID, AMOUNT) VALUES (?, ?) ON DUPLICATE KEY UPDATE AMOUNT=?",
					bankAccount.getId(), bankAccount.getAmount(), bankAccount.getAmount());
		}
	}
}
