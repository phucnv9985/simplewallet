**How to run program**

1. Import **Source** to **Eclipse IDE** as Maven project.

---

## Config files


1. **src/main/resources/application.properties**: fill in your database info (database Mysql)
2. **src/main/resources/data.sql**: sql to create table and  insert sample data

---

## Testcase file

MoneyTransferServiceTest.java